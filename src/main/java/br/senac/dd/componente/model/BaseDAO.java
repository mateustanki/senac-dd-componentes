/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.dd.componente.model;

import java.sql.SQLException;

/**
 *
 * @author Aluno
 */
public interface BaseDAO <A, B> {
        
    public A getPorId(B id) throws SQLException;
    public boolean excluir(B id) throws SQLException;
    public boolean alterar(A object) throws SQLException;
    public B inserir(A object) throws SQLException;
    
    
}
