
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.dd.componentes.exception;



public class ParametrosInvalidosException extends RuntimeException{
    
private Integer numeroErro;

    public Integer getNumeroErro() {
        return numeroErro;
    }

    public void setNumeroErro(Integer numeroErro) {
        this.numeroErro = numeroErro;
    }

    public ParametrosInvalidosException() {
    }

    public ParametrosInvalidosException(String message) {
        super(message);
    }

    public ParametrosInvalidosException(String message, Throwable cause) {
        super(message, cause);
    }

    public ParametrosInvalidosException(Throwable cause) {
        super(cause);
    }

    public ParametrosInvalidosException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public ParametrosInvalidosException(Integer numeroErro) {
        this.numeroErro = numeroErro;
    }

    public ParametrosInvalidosException(Integer numeroErro, String message) {
        super(message);
        this.numeroErro = numeroErro;
    }

    public ParametrosInvalidosException(Integer numeroErro, String message, Throwable cause) {
        super(message, cause);
        this.numeroErro = numeroErro;
    }

    public ParametrosInvalidosException(Integer numeroErro, Throwable cause) {
        super(cause);
        this.numeroErro = numeroErro;
    }

    public ParametrosInvalidosException(Integer numeroErro, String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.numeroErro = numeroErro;
    }
    
    

  
}
