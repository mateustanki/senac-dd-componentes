/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.senac.dd.componentes.db;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UtilSQL {
    private static final SimpleDateFormat fmtDataTempo = 
                new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private static final SimpleDateFormat fmtData = 
                new SimpleDateFormat("yyyy-MM-dd");
    
    public static String getDataTempoToSQL(Date data){
        if(data == null)
            return "null";
        return "{ts '" + fmtDataTempo.format(data) + "'} ";
    }
        
    public static String getDataToSQL(Date data){
        if(data == null)
            return "null";
        return "{d '" + fmtData.format(data) + "'} ";
    }

    
}
